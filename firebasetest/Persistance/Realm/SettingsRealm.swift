//
//  SettingsRealm.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit
import RealmSwift

/**
 This persistence class for the RealmDB provides `properties` for the `Settings` model class

 ## Realm Migration Versions
 See [Realm documentation for migration](https://realm.io/docs/swift/latest/#migrations)

 ### Version 0
 filtercurrentYear = true
 iniitial = true
 personsListDisplayTypeRaw = 0

 - SeeAlso `Settings`, `SettingsBackend`, `SettingsProtocol`, `BackendModelProtocol`
 */
class SettingsRealm: Object, BackendModelProtocol, SettingsProtocol {

    var uuid = ""

    /// Represents a flag if the user has accepted the disclaimer or not
    dynamic var disclaimerAccepted : Bool = false

    /// The initial property indicates, if a backend object was persisted to the database or not. The initial value is set to `true`.
    dynamic var initial = true

    //    override static func ignoredProperties() -> [String] {
    //        return [""]
    //    }
}
