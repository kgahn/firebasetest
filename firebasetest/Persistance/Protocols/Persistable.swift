//
//  Persistable.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/**
 The `Persistable` protocol defines the contract for model objects which need **backend integration** (like a database and its corresbonding backend model objects).

 Therefore 2 functions need to be implemented:

 1. `updateBackendModel() -> BackendModelProtocol` and
 2. `func updateFromBackendModel(backendModel: BackendModelProtocol)`

 The idea behind that contract is to really separate the layers completely by protocols, so that updating or receiving an update happens inside the model and can be triggered when needed (as opposite to automatically persiste a model object on value changes).

 - SeeAlso: `BackendModelProtocol`

 */
protocol Persistable {

    /// A reference to the backend model object to be used with the implementing model object
    var backendModel: BackendModelProtocol? {get set}

    var uuid: String {set get}

    /// The only function to use when updating the backend model object
    func updateBackendModel() -> BackendModelProtocol

    /// The only function to use when receiving updated backend model object data and the current model object **does not have the backend model already injected** as the implementation of this function must inject the passed in `backendModel` as 'the new' backend model object.
    func updateFromBackendModel(backendModel: BackendModelProtocol)

    // The only function to use when receiving updated backend model object data and the current model object **already has a backend model object* injected. This function only updates/remappes the fields.
    func updateFromBackendModel()
}
