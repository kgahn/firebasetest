//
//  SettingsBackend.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

protocol SettingsBackend: SettingsProtocol, BackendModelProtocol {

}
