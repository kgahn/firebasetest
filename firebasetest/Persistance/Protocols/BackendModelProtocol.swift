//
//  BackendModelProtocol.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/**
 The `BackendModelProtocol` needs to be implemented by all classes which should get persisted to any underlying *backend layer*.

 Actually this protocol defines the following contract:

  - `initial`: This indicates, if the business object already was persisted (= `true`) or if it is still in an initial state (= `false`) from a backend perspective.
 */
protocol BackendModelProtocol {

    var uuid: String {set get}

    /// This indicates, if the business object already was persisted (= `true`) or if it is still in an initial state (= `false`) from a backend perspective.
    var initial: Bool { get set }

//    init(uuid: String)

}
