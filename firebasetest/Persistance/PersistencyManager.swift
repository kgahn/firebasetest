//
//  PersistencyManager.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase

/**
 The `PersistencyManager`class provides all concrete backend implementations for the business layer. In the current implementation the main backend persistance layer is `RealmSwift` with the Realm database as database layer.

 - SeeAlso: www.realm.io for database access documentation

###WRITE Example

 ```
func writeRegistration(person: Person) {
     let photoShoot = PhotoShootRealm()
     let realm = try! Realm()
     try! realm.write {
        realm.add(photoShoot)
     }
}
 ```

###READ Example

 ```
func readRegistrations() -> [PhotoShootingEntry] {
    let realm = try! Realm()
    let realmList = realm.objects(PhotoShootRealm)
    var entryList = [PhotoShootingEntry]`()`
    for realmEntry in realmList {
        let entry = PhotoShootingEntry()
        entry.firstName = realmEntry.firstName
    }
    return entryList
 }
 ```

###DELETE Example

 ```
func deleteEntry (entry: PhotoShootingEntry) {
    let realm = try! Realm()
    try! realm.write {
        realm.delete(entry.backend!)
    }
}
 ```

###UPDATE Example

```
func updateSettings(settings: Settings) {
    let realm = try! Realm()
    try! realm.write {
        let settingsBackend = settings.settingsBackend
        settingsBackend?.inital = settings.initial
    }
}
```
 */

class PersistencyManager: NSObject {

    class var sharedInstance: PersistencyManager {
        /// A singleton representation of the `PersistencyManager` class
        struct Singleton {
            /// instance represents internally the Singleton
            static let instance = PersistencyManager()
        }

        return Singleton.instance
    }
    
    var ref: FIRDatabaseReference = FIRDatabase.database().reference()

    // MARK: - Settings methods
    func writeOrUpdateSettings(settings: Settings) {
        if(settings.backendModel?.initial == true) {
            writeSettings(settings: settings)
        } else {
            updateSettings(settings: settings)
        }
    }

    func updateSettings(settings: Settings)  {
        let realm = try! Realm()
        try! realm.write {

            let settingsRealm = settings.backendModel as! SettingsRealm

            settingsRealm.disclaimerAccepted = settings.disclaimerAccepted
        }

    }

    // TODO: refactor the attributes mapping as no longer needed
    func writeSettings(settings: Settings) {
        let realm = try! Realm()
        let settingsRealm = settings.backendModel as! SettingsRealm

        // Attributes mapping
        settingsRealm.disclaimerAccepted = settings.disclaimerAccepted

        settingsRealm.initial = false;
        try! realm.write {
            realm.add(settingsRealm)
        }
    }

    // TODO: refactor the attributes mapping as no longer needed
    func readSettings() -> Settings {
        let realm = try! Realm()

        let realmList = realm.objects(SettingsRealm)

        if(realmList.count == 0) {
            let settingsRealm = SettingsRealm()
            let settings = Settings(withBackendModel: settingsRealm)
            writeSettings(settings: settings)
            readSettings()
        }
        let settingsRealm = realmList.first!
        let settings = Settings(withBackendModel: settingsRealm)

        // Attributes mapping
        settings.disclaimerAccepted = settingsRealm.disclaimerAccepted

        return settings
    }

    // MARK: - Firebase
    func readUsers() {
        // Read users with happenings
        ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let userList = snapshot.value as? NSDictionary
            let libraryAPI = LibraryAPI.sharedInstance
            
            for (key,value) in userList! {
                var newUser = UserFirebase()
                newUser.createFrom(firebaseDict: value as! NSDictionary)
                libraryAPI.users.append(newUser)
            }
           // ...
        }) { (error) in
            print(error.localizedDescription)
        }
    }

}
