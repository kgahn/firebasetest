//
//  MainViewController.swift
//  firebasetest
//
//  Created by Gahn, Karsten on 14.02.17.
//  Copyright © 2017 zfd. All rights reserved.
//

import UIKit
import Firebase

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let libraryAPI = LibraryAPI.sharedInstance
    private let modelManager = LibraryAPI.sharedInstance.modelManager
    
    var ref: FIRDatabaseReference!
    fileprivate var _refHandle: FIRDatabaseHandle?
    fileprivate var _refHandleHappenings: FIRDatabaseHandle?
    var users: [FIRDataSnapshot]! = []
    var userHappenings: [FIRDataSnapshot]! = []
    
    var happenings: [FIRDataSnapshot]! = []

    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var happeningsTableView: UITableView!
    @IBOutlet weak var userHappeningsTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureDatabase() 

        // Do any additional setup after loading the view.
    }
    
    func configureDatabase() {
        ref = FIRDatabase.database().reference()
        // Listen for new x in the Firebase database
        _refHandle = self.ref.child("users").observe(.childAdded, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.users.append(snapshot)
            strongSelf.userTableView.insertRows(at: [IndexPath(row: strongSelf.users.count-1, section: 0)], with: .automatic)
        })
        
        _refHandle = self.ref.child("happenings").observe(.childAdded, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            strongSelf.happenings.append(snapshot)
            strongSelf.happeningsTableView.insertRows(at: [IndexPath(row: strongSelf.happenings.count-1, section: 0)], with: .automatic)
        })
    }

    deinit {
        if let refHandle = _refHandle  {
            self.ref.child("users").removeObserver(withHandle: refHandle)
        }
        if let refHandleHappenings = _refHandleHappenings  {
            self.ref.child("happenings").removeObserver(withHandle: refHandleHappenings)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == userTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserTableViewCell
            
            // Unpack message from Firebase DataSnapshot
            let userSnapshot: FIRDataSnapshot! = self.users[indexPath.row]
            guard let user = userSnapshot.value as? [String:Any] else { return cell }
            let firstName = user["first-name"] ?? ""
            
            cell.firstName.text = firstName as? String
            
            let lastName = user["last-name"] ?? ""
            
            cell.lastName.text = lastName as? String
            return cell

        } else if tableView == userHappeningsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "userHappeningsCell", for: indexPath) as! UserHappeningsTableViewCell
            // Unpack message from Firebase DataSnapshot
            let happeningSnapshot: FIRDataSnapshot! = self.userHappenings[indexPath.row]
            guard let happening = happeningSnapshot.value as? [String:Any] else { return cell }
            let name = happening["name"] ?? ""
            cell.name.text = name as? String
            
            let hid = happening["happening-id"] ?? ""
            cell.hid.text = hid as? String
            
            let url = happening["url"] ?? ""
            cell.url.text = url as? String
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "happeningCell", for: indexPath) as! HappeningTableViewCell
            
            // Unpack message from Firebase DataSnapshot
            let happeningSnapshot: FIRDataSnapshot! = self.happenings[indexPath.row]
            guard let happening = happeningSnapshot.value as? [String:Any] else { return cell }
            let name = happening["name"] ?? ""
            
            cell.happeningTitle.text = name as? String
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == userTableView {
            return users.count
        } else if tableView == userHappeningsTableView {
            return userHappenings.count
        }
        else {
            return happenings.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == userTableView {
            userHappenings.removeAll()
            let selection = users[indexPath.row]
            guard let selectedUser = selection.value as? [String:Any] else { return }
            let selectedUserHappenings = selectedUser["happenings"] as? NSDictionary ?? NSDictionary()
            
            print("Number of happenings: \(selectedUserHappenings.count)")
            for (key,value) in selectedUserHappenings {
                let happeningId = key
                ref.child("happenings").child(happeningId as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    self.userHappenings.append(snapshot)
                    // Get user value
//                    let value = snapshot.value as? NSDictionary
//                    let name = value?["name"] as? String ?? ""
//                    let url = value?["url"] as? String ?? ""
                    // ...
                    self.userHappeningsTableView.reloadData()
                    self.userHappeningsTableView.setNeedsLayout()
                    self.userHappeningsTableView.setNeedsDisplay()
                }) { (error) in
                    print(error.localizedDescription)
                }
            }
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
