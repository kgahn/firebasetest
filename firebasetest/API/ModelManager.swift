//
//  ModelManager.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/**
 The ModelManager takes care of all business object relevant things. It is important to let the ModelManager create model objects which should have backend access.

 The ModelManager in general provides a central (singleton) point of access for all model objects logic relevant things.

 ## Why create models via this manager
 The reason for this is the following: We wanted to decouple direct backend integration into the model itself. Therefore only a protocol `BackendModelProtocol` is used. If now a model should be created with backend layer access, the model needs to provide the following:

 1. Implementation of the `Persistable` protocol
 2. Implementation of a protocol specific for the model class itself. This protocol later has to be implemented by the ModelPersistance class too.

## Model Example

```
class Settings: SettingsProtocol, Persistable {

 var backendModel: BackendModelProtocol?

 var disclaimerAccepted = false

 /// Initializes the model with the passed in `BackendModelProtocol` based backend model object.
 init(withBackendModel backendModel: BackendModelProtocol) {
 self.backendModel = backendModel
 }

}
```

 Although `Settings`is a special case, the principle is always the same.

## Settings object

 `Settings` is a standard object of all zuerich-fresh-design Realm based projects. It
 */
class ModelManager: NSObject {

   private let persistencyManager = PersistencyManager.sharedInstance

    var settings:Settings = Settings.init(withBackendModel: SettingsRealm())


//    /// Returns an initialized `Person` object with embedded backend model object reference to its `PersonRealm` implementation.
//    func getInitializedPerson() -> Person {
//        let model = Person(withBackendModel: PersonRealm())
//
//        return model
//    }
//
//    func createOrUpdatePerson(person: Person) {
//        persistencyManager.createOrUpdatePerson(person)
//    }


}
