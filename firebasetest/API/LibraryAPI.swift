//
//  LibraryAPI.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/** The LibraryAPI in general provides a central (singleton) point of access for all business logic relevant things.

 Two main references are provided by the class:

 - a Model manager (`ModelManager`) and
 - a Persistancy Manager (`PersistencyManager`)

 The `ModelManager` can be accessed by the 'modelManager' property whereas there is no direct API access to the persistencyManager. To get access, you should add your methods needed like `saveRide()` and from within this method access the corresponding backend methods of the PersistencyManager like `persistencyManager.saveRide(ride: Ride)`.

 - SeeAlso: `ModelManager`
 - SeeAlso: `Settings`
 - SeeAlso: `PersistencyManager`
 */
open class LibraryAPI: NSObject {

    let modelManager: ModelManager
    
    var users = [UserFirebase]()
    
    fileprivate let persistencyManager = PersistencyManager.sharedInstance

    class var sharedInstance: LibraryAPI {
        /// A singleton representation of the `LibraryAPI` class
        struct Singleton {
            /// instance represents internally the Singleton
            static let instance = LibraryAPI()
        }

        return Singleton.instance
    }

    override init() {
        modelManager = ModelManager()
        super.init()

    }

    // MARK: - API methods
    func saveSettings() {
        persistencyManager.writeOrUpdateSettings(settings: modelManager.settings)
    }

    func readSettings() -> Settings {
        modelManager.settings = persistencyManager.readSettings()
        return modelManager.settings
    }

}
