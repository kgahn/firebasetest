//
//  UIView+RoundCorners.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

extension UIView {

    /// Creates round corners for a `UIView`.
    ///
    /// If you set `radius = nil` this sets the radius to the max (circle, if a square).
    ///
    /// All parameters are are optional with default values. Per default: `radius` is `nil`, `corners` is `UIRectCorner.allCorners` and there is no border.
    ///
    /// If you like to have individual corners styled, pass them as an array like `[.UIRectCorner.topLeft, .UIRectCorner.bottomRight]`.
    ///
    /// - Parameters:
    ///     - radius: Corner radius. Default = `nil` which is maxium radius.
    ///     - corners: An array of `UIRectCorner`. Default is `UIRectCorner.allCorners`
    ///     - borderWidth: Defines the size of the border. Default is `0`
    ///     - borderColor: This is the color of the border. Default is `UIColor.clear`
    func round(radius: CGFloat? = nil, corners: UIRectCorner = UIRectCorner.allCorners, borderWidth: CGFloat = 0, borderColor: UIColor = UIColor.clear) {

        var radius = radius
        let rect = self.bounds;
        if radius == nil {
            let width = self.frame.width
            let height = self.frame.height

            radius = width >= height ? height / 2 : width / 2
        }

        let maskPath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius!, height: radius!))

        // Create the shape layer and set its path
        let maskLayer = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path  = maskPath.cgPath

        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer

        //Create path for border
        let borderPath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius!, height: radius!))

        // Create the shape layer and set its path
        let borderLayer = CAShapeLayer()

        borderLayer.frame       = rect
        borderLayer.path        = borderPath.cgPath
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.fillColor   = UIColor.clear.cgColor
        borderLayer.lineWidth   = borderWidth * UIScreen.main.scale

        //Add this layer to give border.
        self.layer.addSublayer(borderLayer)


    }
}
