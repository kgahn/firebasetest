//
//  String+CharacterAtIndex.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import Foundation

/**
 This extension provides the functionality, that you can get a `Character` out of an existing String at a certain index.

 ## Stackoverflow solution
 See [stackoverflow.com](http://stackoverflow.com/questions/24092884/get-nth-character-of-a-string-in-swift-programming-language)

 - SeeAlso `String`, `Character`
 */
extension String {
    subscript(pos: Int) -> String {
        precondition(pos >= 0, "character position can't be negative")
        guard pos < characters.count else { return "" }
        let idx = index(startIndex, offsetBy: pos)
        return self[idx...idx]
    }
    subscript(range: CountableRange<Int>) -> String {
        precondition(range.lowerBound.distance(to: 0) <= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return self[lowerIndex..<(index(lowerIndex, offsetBy: range.upperBound - range.lowerBound, limitedBy: endIndex) ?? endIndex)]
    }
    subscript(range: ClosedRange<Int>) -> String {
        precondition(range.lowerBound.distance(to: 0) <= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return self[lowerIndex..<(index(lowerIndex, offsetBy: range.upperBound - range.lowerBound + 1, limitedBy: endIndex) ?? endIndex)]
    }
}
