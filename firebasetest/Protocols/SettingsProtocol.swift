//
//  SettingsProtocol.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

protocol SettingsProtocol {

    var disclaimerAccepted: Bool {get set}

}
