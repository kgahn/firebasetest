//
//  Happening.swift
//  firebasetest
//
//  Created by Gahn, Karsten on 14.02.17.
//  Copyright © 2017 zfd. All rights reserved.
//

import Foundation

struct Happening {
    var name: String?
    var url: String?
    var id: String?
    
    func toAnyObject() -> Any {
        return [
            "name": name!,
            "url": url!,
            "happening-id": id!
        ]
    }
}
