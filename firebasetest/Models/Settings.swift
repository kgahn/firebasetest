//
//  Settings.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/**
 The Settings class follows the `SettingsProtocol` protocol and the `Persistable` protocol. It provides basic inital functionality out of the box like.

 The Settings model class has all layers 'down to' the Realm database already established.

 1. `disclaimerAccepted` - A `Bool` variable which indicates, if the App Disclaimer already was accepted by the user or not. Initally, it is set to `false`
 2. `personsListDisplayType`- An `PersonsListTypes` variable which holds information about how to display the persons (ex. as list or as grid)

 - SeeAlso: `SettingsRealm`, `SettingsProtocol`, `SettingsBackend`, `Persistable`

 - SeeAlso: `PersonsListTypes`
 */
class Settings: SettingsProtocol, Persistable {


    var uuid = NSUUID().uuidString

    /// The `disclaimerAccepted` variable indicates, if the App Disclaimer already was accepted by the user or not. Initally, it is set to `false`
    var disclaimerAccepted = false

    // Persistable implementation
    /// Reference to the underlying backend model
    var backendModel: BackendModelProtocol?

    /// Initializes the model with the passed in `BackendModelProtocol` based backend model object.
    init(withBackendModel backendModel: BackendModelProtocol) {
        self.backendModel = backendModel
        updateFromBackendModel()
    }

    /// Updates the backend model object as a `SettingsProtocol` object
    func updateBackendModel() -> BackendModelProtocol {
        var bemo = backendModel as! SettingsProtocol

        bemo.disclaimerAccepted = disclaimerAccepted

        return bemo as! BackendModelProtocol
    }

    /// Updates the model object from a `SettingsProtocol` backend model object
    ///
    /// **IMPORTANT:** The only function to use when receiving updated backend model object data and the current model object **does not have the backend model already injected** as the implementation of this function must inject the passed in `backendModel` as 'the new' backend model object.
    func updateFromBackendModel(backendModel: BackendModelProtocol) {
        var bemo = backendModel as! SettingsProtocol
        disclaimerAccepted = bemo.disclaimerAccepted
    }

    /// Updates the model object from the already injected `SettingsProtocol` backend model object.
    ///
    /// **IMPORTANT:** The only function to use when receiving updated backend model object data and the current model object **already has a backend model object* injected. This function only updates/remappes the fields.
    ///
    /// Internally it calls `updateFromBackendModel(backendModel: BackendModelProtocol)`
    func updateFromBackendModel() {
        updateFromBackendModel(backendModel: backendModel!)
    }


}
