//
//  UserFirebase.swift
//  firebasetest
//
//  Created by Gahn, Karsten on 14.02.17.
//  Copyright © 2017 zfd. All rights reserved.
//
import Foundation

class UserFirebase {
    
    static let fbFirstName = "first-name"
    static let fbLastName = "last-name"
    static let fbUsername = "username"
    static let fbUuid = "uuid"
    
    var firstName: String?
    var lastName: String?
    var avatar: NSData?
    var username: String?
    var uuid: String?
    var subscriptions = [Happening]()
    
    func toAnyObject() -> Any {
        var data: [String: Any] =  [
            "first-name": firstName!,
            "last-name": lastName!,
            "username": username!,
            "uuid": uuid!
        ]
        
        if(subscriptions.count > 0) {
            var hData = [String:Any]()
            for happening in subscriptions {
                //hData[happening.id!] = happening.toAnyObject()
                hData[happening.id!] = true
            }
            data["happenings"] = hData
        }
        
        return data
    }
    
    func createFrom(firebaseDict: NSDictionary) {
        for (key,value) in firebaseDict {
            let dataKey = key as! String
            switch dataKey {
            case UserFirebase.fbFirstName:
                firstName = value as? String ?? ""
                break
            case UserFirebase.fbLastName:
                lastName = value as? String ?? ""
                break
            case UserFirebase.fbUsername:
                username = value as? String ?? ""
                break
            case UserFirebase.fbUuid:
                uuid = value as? String ?? ""
            default:
                break
            }
            
        }
    }
}
