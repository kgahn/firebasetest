//
//  RealmHelper.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit
import RealmSwift

/**
 This Helper Class provides helper methods dealing with the RealmSwift implementation
 */
class RealmHelper: NSObject {
    
    /// Deletes the full Realm DB
    static func deleteRealmDB() {
        
        
        // Scrub the REALM DB
        
        let realmURL = Realm.Configuration.defaultConfiguration.fileURL!
        let realmURLs = [
            realmURL,
            realmURL.appendingPathComponent("lock"),
            realmURL.appendingPathComponent("log_a"),
            realmURL.appendingPathComponent("log_b"),
            realmURL.appendingPathComponent("note")
        ]
        let manager = FileManager.default
        for URL in realmURLs {
            do {
                try manager.removeItem(at: URL)
            } catch {
                // handle error
            }
        }
        
    }
    
    /// Returns the `NSURL`of the current Realm configuration database
    static func getRealmPathURL() -> URL {
        return Realm.Configuration.defaultConfiguration.fileURL!
    }
    
}
