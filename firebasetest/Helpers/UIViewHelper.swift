//
//  UIViewHelper.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import UIKit

/**
 The UIViewHelper class provides a number of helpful functions

 Actual they are:

 - functions:
    - setButtonStyles

 - Functions:
    - x: The *x* component of the vector.
    - y: The *y* component of the vector.
    - z: The *z* component of the vector.
*/
class UIViewHelper {
    static let BUTTON_DARK_BLUE_GRAY_COLOR: UIColor = UIColor.init(netHex:0x455154)

    static let BUTTON_DARK_GREY_COLOR: UIColor = UIColor.init(netHex:0x494D50)
    static let BUTTON_GREY_COLOR: UIColor = UIColor.init(netHex:0x5A6366)
    static let BUTTON_LIGHT_GREY_COLOR: UIColor = UIColor.init(netHex:0x7E7F80)

    static let BUTTON_BLUE_COLOR: UIColor = UIColor.init(netHex:0x4996B2)
    static let BUTTON_LIGHT_BLUE_COLOR: UIColor = UIColor.init(netHex: 0x9EB9C3)
    static let BUTTON_BEIGE_COLOR: UIColor = UIColor.init(netHex:0xB2997E)
    static let BUTTON_DARK_BROWN_COLOR: UIColor = UIColor.init(netHex:0x66421A)
    static let BUTTON_LIGHT_BROWN_COLOR: UIColor = UIColor.init(netHex:0xB3784B)
    static let BUTTON_RED_BROWN_COLOR: UIColor = UIColor.init(netHex:0xB35E4B)
    static let BUTTON_ALMOND_COLOR: UIColor = UIColor.init(netHex:0xFFE7CD)
    static let BUTTON_WHITE_COLOR: UIColor = UIColor.init(netHex:0xFFFFFF)

    /**
        Formats the `button` (passed as parameter) as a round button.

        - Parameter button: The button which will get the round corners

        - Parameters:
            - x: The *x* component of the vector.
            - y: The *y* component of the vector.
            - z: The *z* component of the vector.

        Code `example` example

            let height = x

     */
    /// Returns the magnitude of a vector in three dimensions
    /// from the given components.
    ///
    ///
    static func setButtonStyles(button: UIButton, backgroundColor: UIColor?) {
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.layer.borderWidth = 2
        button.titleLabel?.adjustsFontSizeToFitWidth = true

        if let backgroundColor = backgroundColor {
            button.setTitleColor(BUTTON_WHITE_COLOR, for: .normal)
            button.backgroundColor = backgroundColor
            button.layer.borderColor = backgroundColor.cgColor
        } else {
            button.setTitleColor(BUTTON_WHITE_COLOR, for: .normal)
            button.backgroundColor = UIColor.clear
            button.layer.borderColor = BUTTON_WHITE_COLOR.cgColor
        }
    }

    static func styleAvatarImage(avatarImageView: UIImageView, hasAvatar: Bool) {

        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2;
        avatarImageView.clipsToBounds = true;
        avatarImageView.superview
        if(hasAvatar) {
            avatarImageView.contentMode = .scaleAspectFill
        } else {
            avatarImageView.backgroundColor = UIViewHelper.BUTTON_WHITE_COLOR
            avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2;
            avatarImageView.contentMode = .scaleAspectFill
        }

    }
}

// MARK: - Extensions
// Usage:
// var color = UIColor(red: 0xFF, green: 0xFF, blue: 0xFF)
// var color2 = UIColor(netHex:0xFFFFFF)
// http://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values-in-swift-ios
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
