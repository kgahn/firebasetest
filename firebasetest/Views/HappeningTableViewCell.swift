//
//  HappeningTableViewCell.swift
//  firebasetest
//
//  Created by Gahn, Karsten on 14.02.17.
//  Copyright © 2017 zfd. All rights reserved.
//

import UIKit

class HappeningTableViewCell: UITableViewCell {

    @IBOutlet weak var happeningTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
