//
//  UserHappeningsTableViewCell.swift
//  firebasetest
//
//  Created by Gahn, Karsten on 15.02.17.
//  Copyright © 2017 zfd. All rights reserved.
//

import UIKit

class UserHappeningsTableViewCell: UITableViewCell {

    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var hid: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
