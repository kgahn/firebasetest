//
//  AppDelegate.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import Foundation
import RealmSwift
import Firebase

import Fakery




/**
 This AppDelegate class contains additional RealmSwift information.

 ## RealmSwift additions
 ### Realm URL
 It prints out the Realm URL Path.

 ### Delete the RealmDB
 It also contains a commented out line to delete the complete RealmDB.

 `//RealmHelper.deleteRealmDB()`

 ### Migration of RealmDB Versions
 There is also an outcommented migration block inside the `Realm.Configuration.defaultConfiguration = Realm.Configuration(...` coding block. So that you easily have a template for migration to start with.

 - SeeAlso `RealmHelper`
 */
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var ref: FIRDatabaseReference!
    
    

    /// Additional RealmSwift information implemented
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //RealmHelper.deleteRealmDB()
        
        // Migration
        Realm.Configuration.defaultConfiguration = Realm.Configuration()
        // Firebase
        FIRApp.configure()
        
        print("App delegate: \(RealmHelper.getRealmPathURL())")
        
        FIRAuth.auth()?.signIn(withEmail: "karsten@gahn.ch", password: "geheim") { (user, error) in
            if error != nil {
                print(error)
            }
        }
        
        ref = FIRDatabase.database().reference()
        // Generate dummy user data
//        var users = [User]()
//        var happenings = [Happening]()
//
//        let faker = Faker(locale: "de-DE")
//        
//        
//        for _ in 0..<5 {
//            var happening = Happening()
//            happening.name = faker.team.creature()
//            happening.id = UUID.init().uuidString
//            happening.url = faker.internet.url()
//            happenings.append(happening)
//            print(happening.toAnyObject())
//        }
//        
//        for _ in 0..<5 {
//            let rand = Int(arc4random_uniform(5))
//            var user = User()
//            user.username = faker.internet.safeEmail()
//            user.firstName = faker.name.firstName()
//            user.lastName = faker.name.lastName()
//            user.uuid = UUID.init().uuidString
//            
//            
//            
//            let eventNo = Int(arc4random_uniform(3))
//            for _ in 0...eventNo {
//                user.subscriptions.append(happenings[Int(arc4random_uniform(5))])
//            }
//            
//            users.append(user)
//            print(user.toAnyObject())
//        }
//        
//        
//        // Write users to Firebase
//        for user in users {
//            
//            let node = ref.child("users").child(user.uuid!)
//            node.setValue(user.toAnyObject())
//        }
//        
//        for happening in happenings {
//            let node = ref.child("happenings").child(happening.id!)
//            node.setValue(happening.toAnyObject())
//            
//        }
        
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
