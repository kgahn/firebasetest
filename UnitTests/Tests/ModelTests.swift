//
//  ModelTests.swift
//  firebasetest
//
//  Created by Karsten Gahn on 2/14/17
//  Copyright © 2017 Karsten Gahn, zfd. All rights reserved.
//

import XCTest
import RealmSwift
import UIKit
@testable import TOi

class ModelTests: RealmTestCaseBase {

    var libraryAPI: LibraryAPI?
    var modelManager = LibraryAPI.sharedInstance.modelManager

    override func setUp() {
        super.setUp()
        libraryAPI = LibraryAPI.sharedInstance
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // MARK: - Tests for LibraryAPI
    func testLibraryAPI_NotNil() {
        XCTAssertNotNil(libraryAPI, "LibraryAPI should not be nil")
    }

    func testModelManager_ShouldNotBeNil() {
        let modelManager = LibraryAPI.sharedInstance.modelManager
        XCTAssertNotNil(modelManager, "ModelManager should not be nil")
    }
}
